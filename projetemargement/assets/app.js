// app.js
//styles css
import './styles/app.scss'
import './styles/header.scss'
import './styles/undernavigation.scss'
import './styles/form.scss'
import './styles/bouton.scss'

//image 
import './images/marge.png'

// pages scss
import './pages/home.scss'

// font
import './font/font.scss'

const $ = require('jquery');
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require('bootstrap');

// or you can include specific pieces
// require('bootstrap/js/dist/tooltip');
// require('bootstrap/js/dist/popover');

$(document).ready(function() {
    $('[data-toggle="popover"]').popover();
});

