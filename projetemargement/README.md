# Projet Signature
Symfony j'adore sa ! 

## Setup project
```bash
composer install
yarn install
```

## Compiles for development
```bash
yarn watch
```

### Cache clearing
```bash
php bin/console cache:clear
php bin/console doctrine:cache:clear-metadata
php bin/console doctrine:cache:clear-query
php bin/console doctrine:cache:clear-result
```

### Schema
```bash
php bin/console doctrine:schema:validate
php bin/console doctrine:schema:update --force --complete --dump-sql
```
