<?php


namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AdminFixtures
 * @package App\DataFixtures
 *
 * @author Grégoire Ciles
 */
class AdminFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoderInterface;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userPasswordEncoderInterface = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadAdmin($manager);
    }

    /**
     * Génère 1 Responsable
     *
     * @param $manager
     * @throws \Exception
     */
    private function loadAdmin($manager)
    {
        $user = new User();
        $user
            ->setFirstName("Mélanie")
            ->setLastName("Engrand")
            ->setEmail("meme.engrand@gmail.com")
            ->setPassword($this->userPasswordEncoderInterface->encodePassword($user, "melanie4"))
            ->setRoles(['ROLE_ADMIN']);
        $this->setReference("admin", $user);
        $manager->persist($user);
        $manager->flush();
    }
}
