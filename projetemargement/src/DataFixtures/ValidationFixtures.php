<?php


namespace App\DataFixtures;

use App\Entity\Validation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class ValidationFixtures
 * @package App\DataFixtures
 *
 * @author Grégoire Ciles
 */
class ValidationFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadSignature($manager);
    }

    /**
     * @param $manager
     */
    private function loadSignature($manager) {
        for($i = 1; $i < 10; $i++) {
            $signature = (new Validation())
                ->setUser($this->getReference("apprenant$i"))
                ->setTimeslot($this->getReference('slot01'))
                ->setPresence('absent')
            ;
            $manager->persist($signature);
        }
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            AdminFixtures::class,
            TrainingFixtures::class,
            UserFixtures::class,
            TimeSlotFixtures::class,
        ];
    }

}
