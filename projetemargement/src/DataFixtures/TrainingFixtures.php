<?php


namespace App\DataFixtures;

use App\Entity\Promotion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class TrainingFixtures
 * @package App\DataFixtures
 *
 * @author Grégoire Ciles
 */
class TrainingFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadTraining($manager);
    }

    /**
     * @param $manager
     */
    private function loadTraining($manager)
    {
        $training = (new Promotion())
            ->setPromotionName('Promo 01')
            ->setAuthor($this->getReference("admin"));
        $this->setReference('Training01', $training);
        $manager->persist($training);
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            AdminFixtures::class,
        ];
    }
}
