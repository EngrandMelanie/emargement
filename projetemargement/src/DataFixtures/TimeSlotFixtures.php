<?php


namespace App\DataFixtures;

use App\Entity\TimeSlot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * Class TimeSlotFixtures
 * @package App\DataFixtures
 *
 * @author Grégoire Ciles
 */
class TimeSlotFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->loadTimeSlot($manager);
    }

    /**
     * @param $manager
     * @throws \Exception
     */
    private function loadTimeSlot($manager) {
        $timeslot = (new TimeSlot())
            ->setStart(new \DateTime())
            ->setEnd(new \DateTime())
            ->setPromotion($this->getReference('Training01'))
            ->setCode(random_int(1000, 9999))
        ;

        $this->setReference('slot01', $timeslot);
        $manager->persist($timeslot);
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            AdminFixtures::class,
            TrainingFixtures::class,
            UserFixtures::class,
        ];
    }
}
