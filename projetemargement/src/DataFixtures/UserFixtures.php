<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 *
 * @author Grégoire Ciles
 */
class UserFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $userPasswordEncoderInterface;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->userPasswordEncoderInterface = $passwordEncoder;
    }

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadManager($manager);
    }

    /**
     * Génère 10 Apprenants
     *
     * @param $manager
     * @throws \Exception
     */
    private function loadUsers($manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User;
            $user
                ->setFirstName("Firstname-$i")
                ->setLastName("Lastname-$i")
                ->setEmail("apprenant$i@domain.fr")
                ->setPassword($this->userPasswordEncoderInterface->encodePassword($user, "apprenant$i"))
                ->setRoles(['ROLE_USER'])
                ->addPromotion($this->getReference('Training01'));
            $this->setReference("apprenant$i", $user);
            $manager->persist($user);
        }
        $manager->flush();
    }

    /**
     * Génère 1 Formateur
     *
     * @param $manager
     * @throws \Exception
     */
    private function loadManager($manager)
    {
        $user = new User();
        $user
            ->setFirstName("Stéphane")
            ->setLastName("Soulet")
            ->setEmail("stephane@soulet.fr")
            ->setPassword($this->userPasswordEncoderInterface->encodePassword($user, "stephane4"))
            ->setRoles(['ROLE_MANAGER'])
            ->addPromotion($this->getReference('Training01'));
        $this->setReference("formateur", $user);
        $manager->persist($user);
        $manager->flush();
    }

    /**
     * @return string[]
     */
    public function getDependencies(): array
    {
        return [
            AdminFixtures::class,
            TrainingFixtures::class,
        ];
    }
}
