<?php

namespace App\Security\Voter;

use App\Entity\User;
use App\Entity\Promotion;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class FormationEditVoter extends Voter
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    const CREATE = 'trainingCreate';
    const UPDATE = 'trainingUpdate';
    const UPDATE_TRAINING = 'UPDATE_TRAINING';
    const DELETE_TRAINING = 'DELETE_TRAINING';

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [
            self::CREATE,
            self::UPDATE,
            self::UPDATE_TRAINING,
            self::DELETE_TRAINING,
        ]);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::UPDATE_TRAINING:
            case self::DELETE_TRAINING:
                return $this->canUpdatePromotion($user, $subject);
            case self::CREATE:
            case self::UPDATE:
                if($this->security->isGranted('ROLE_ADMIN')) {
                    return true;
                }
        }

        return false;
    }

    private function canUpdatePromotion(User $user, $Promotion): bool
    {
        return $Promotion->getAuthor()->getId() === $user->getId();
    }
}
