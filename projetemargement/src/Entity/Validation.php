<?php

namespace App\Entity;

use App\Repository\ValidationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ValidationRepository::class)
 */
class Validation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Presence;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="validations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=TimeSlot::class, inversedBy="validations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $TimeSlot;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPresence(): ?string
    {
        return $this->Presence;
    }

    public function setPresence(string $Presence): self
    {
        $this->Presence = $Presence;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTimeSlot(): ?TimeSlot
    {
        return $this->TimeSlot;
    }

    public function setTimeSlot(?TimeSlot $TimeSlot): self
    {
        $this->TimeSlot = $TimeSlot;

        return $this;
    }

}
