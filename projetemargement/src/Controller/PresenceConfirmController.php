<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PresenceConfirmController extends AbstractController
{
    /**
     * @Route("/presence/confirm", name="presence_confirm")
     */
    public function index(): Response
    {
        return $this->render('presence_confirm/index.html.twig', [
            'controller_name' => 'PresenceConfirmController',
        ]);
    }
}
