<?php

namespace App\Controller;

use App\Entity\TimeSlot;
use App\Form\TimeSlotType;
use App\Repository\TimeSlotRepository;
use App\Repository\ValidationRepository;
use DateInterval;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TimeSlotController
 * @package App\Controller
 *
 * @Route("/time_slot")
 *
 * @author Mélanie Engrand
 */
class TimeSlotController extends AbstractController
{
    /**
     * @Route("/", name="time_slot_index", methods={"GET"})
     * @IsGranted ("ROLE_MANAGER")
     * @param TimeSlotRepository $timeSlotRepository
     * @return Response
     */
    public function index(TimeSlotRepository $timeSlotRepository): Response
    {
        return $this->render('time_slot/index.html.twig', [
            'time_slots' => $timeSlotRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="time_slot_new", methods={"GET","POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function new(Request $request): Response
    {
        /*dump($request->request->get("time_slot")['Repeat']);*/
        $form = $this->createForm(TimeSlotType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $start = $data["Start"];
            $end = $data["End"];
            $promo = $data["Promotion"];
            $repeat = $request->request->get("time_slot")['Repeat'];
            $repeat++;
            for ($i = 0; $i < $repeat; $i++) {
                $timeSlot = new TimeSlot();
                $entityManager = $this->getDoctrine()->getManager();
                $code = random_int(10000, 99999);
                $timeSlot->setCode($code);
                $timeSlot->setStart($start);
                $timeSlot->setEnd($end);
                $timeSlot->setPromotion($promo);
                $entityManager->persist($timeSlot);
                $entityManager->flush();
                $start->add(new DateInterval('P7D'));
                $end->add(new DateInterval('P7D'));
            }

            return $this->redirectToRoute('time_slot_index');
        }

        return $this->render('time_slot/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="time_slot_show", methods={"GET"})
     * @IsGranted ("ROLE_MANAGER")
     * @param int $id
     * @param TimeSlot $timeSlot
     * @param ValidationRepository $validationRepository
     * @param TimeSlotRepository $timeSlotRepository
     * @return Response
     */
    public function show(int $id, TimeSlot $timeSlot, ValidationRepository $validationRepository, TimeSlotRepository $timeSlotRepository): Response
    {
        $timeSlot = $timeSlotRepository->find($id);
        $timeslotvalidation = $validationRepository->findByValidationForTimeslot($timeSlot);
        return $this->render('time_slot/show.html.twig', [
            'time_slot'          => $timeSlot,
            'timeslotvalidation' => $timeslotvalidation
        ]);
    }

    /**
     * @Route("/{id}/edit", name="time_slot_edit", methods={"GET","POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @param TimeSlot $timeSlot
     * @return Response
     */
    public function edit(Request $request, TimeSlot $timeSlot): Response
    {
        $form = $this->createForm(TimeSlotType::class, $timeSlot);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('time_slot_index');
        }

        return $this->render('time_slot/edit.html.twig', [
            'time_slot' => $timeSlot,
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="time_slot_delete", methods={"POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @param TimeSlot $timeSlot
     * @return Response
     */
    public function delete(Request $request, TimeSlot $timeSlot): Response
    {
        if ($this->isCsrfTokenValid('delete' . $timeSlot->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($timeSlot);
            $entityManager->flush();
        }

        return $this->redirectToRoute('time_slot_index');
    }
}
