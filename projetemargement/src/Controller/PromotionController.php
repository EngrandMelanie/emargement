<?php

namespace App\Controller;

use App\Entity\Promotion;
use App\Form\PromotionType;
use App\Repository\PromotionRepository;
use App\Repository\UserRepository;
use App\Security\Voter\FormationEditVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PromotionController
 * @package App\Controller
 *
 * @Route("/promotion")
 * @IsGranted ("ROLE_ADMIN")
 *
 * @author Mélanie Engrand
 */
class PromotionController extends AbstractController
{
    /**
     * @Route("/", name="promotion_index", methods={"GET"})
     * @IsGranted ("ROLE_ADMIN")
     * @param PromotionRepository $promotionRepository
     * @return Response
     */
    public function index(PromotionRepository $promotionRepository): Response
    {
        return $this->render('promotion/index.html.twig', [
            'promotions' => $promotionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="promotion_new", methods={"GET","POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $this->denyAccessUnlessGranted(FormationEditVoter::CREATE);

        $promotion = new Promotion();
        $me = $this->getUser();

        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $promotion->setAuthor($me);
            $entityManager->persist($promotion);
            $entityManager->flush();

            return $this->redirectToRoute('promotion_index');
        }

        return $this->render('promotion/new.html.twig', [
            'promotion' => $promotion,
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="promotion_show", methods={"GET"})
     * @IsGranted ("ROLE_ADMIN")
     * @param int $id
     * @param Promotion $promotion
     * @param UserRepository $userRepository
     * @param PromotionRepository $promotionRepository
     * @return Response
     */
    public function show(int $id, Promotion $promotion, UserRepository $userRepository, PromotionRepository $promotionRepository): Response
    {
        $promo = $promotionRepository->find($id);
        $usersPromo = $userRepository->findByFormationForPerson($promo);

        return $this->render('promotion/show.html.twig', [
            'promotion'  => $promotion,
            'usersPromo' => $usersPromo
        ]);
    }

    /**
     * @Route("/{id}/edit", name="promotion_edit", methods={"GET","POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @param Promotion $promotion
     * @return Response
     */
    public function edit(Request $request, Promotion $promotion): Response
    {
        $this->denyAccessUnlessGranted(FormationEditVoter::UPDATE);
        $this->denyAccessUnlessGranted(FormationEditVoter::UPDATE_TRAINING, $promotion);

        $form = $this->createForm(PromotionType::class, $promotion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('promotion_index');
        }

        return $this->render('promotion/edit.html.twig', [
            'promotion' => $promotion,
            'form'      => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="promotion_delete", methods={"POST"})
     * @IsGranted ("ROLE_ADMIN")
     * @param Request $request
     * @param Promotion $promotion
     * @return Response
     */
    public function delete(Request $request, Promotion $promotion): Response
    {
        if ($this->isCsrfTokenValid('delete' . $promotion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($promotion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('promotion_index');
    }
}
