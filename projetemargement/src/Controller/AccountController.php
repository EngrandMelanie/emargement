<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 *
 * @author Mélanie Engrand
 */
class AccountController extends AbstractController
{
    /**
     * @Route("/profil", name="auth_profil")
     * @return Response
     */
    public function index(): Response
    {
        $user = $this->getUser();
        return $this->render('user/view.html.twig', ['user' => $user]);
    }
}
