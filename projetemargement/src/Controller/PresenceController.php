<?php

namespace App\Controller;

use App\Entity\TimeSlot;
use App\Entity\User;
use App\Entity\Validation;
use App\Form\SignatureType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class PresenceController
 * @package App\Controller
 *
 * @author Mélanie Engrand
 */
class PresenceController extends AbstractController
{
    /**
     * @Route("/presence", name="presence")
     * @param Request $request
     * @param TokenStorageInterface $token
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function index(Request $request, TokenStorageInterface $token, EntityManagerInterface $manager): Response
    {
        $userId = $token->getToken()->getUser()->getId();
        $user = $this->getDoctrine()->getRepository(User::class)->find($userId);
        $form = $this->createForm(SignatureType::class);
        $form->handleRequest($request);
        $validation = new Validation();
        $validation->setPresence("Absent");
        if ($form->isSubmitted() && $form->isValid()) {
            $timeslotId = $form->getData()['TimeSlot'];
            $timeslot = $this->getDoctrine()->getRepository(TimeSlot::class)->find($timeslotId);

            if (null !== $timeslot) {
                $sameValidationResult = $this->getDoctrine()->getRepository('App\Entity\Validation')->findOneBy(['TimeSlot' => $timeslot, 'user' => $user]);

                if ($sameValidationResult) {
                    $validation->setUser($user);
                    $validation->setTimeSlot($timeslot);

                    if ($form->getData()['Code'] === $timeslot->getCode()) {
                        $validation->setPresence("Present");
                    }
                    $manager->persist($validation);
                    $manager->flush();

                    return $this->redirectToRoute('presence_confirm');
                }
            }
        }


        return $this->render('presence/index.html.twig', [
            'form' => $form->createView(),
            /*'controller_name' => 'PresenceController',*/
        ]);
    }
}
