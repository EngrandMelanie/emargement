<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ConfirmationMessageController
 * @package App\Controller
 *
 * @author Mélanie Engrand
 */
class ConfirmationMessageController extends AbstractController
{
    /**
     * We create a function that will display a message after the recording
     *
     * @Route("/confirmationmessage", name="confirmationmessage")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('home_page/index.html.twig', [
            'controller_name' => 'ConfirmationMessageController',
            'LogInType'       => 'LogInType'
        ]);
    }
}

