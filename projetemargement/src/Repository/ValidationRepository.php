<?php

namespace App\Repository;

use App\Entity\TimeSlot;
use App\Entity\Validation;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Validation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Validation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Validation[]    findAll()
 * @method Validation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ValidationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Validation::class);
    }

    /**
     * @param TimeSlot $timeslot
     * @return array|null
     */
    public function findByValidationForTimeslot(TimeSlot $timeslot): ?array
    {
        return $this->createQueryBuilder('v')
            ->leftJoin('v.TimeSlot', 't')
            ->where('t.id LIKE :timeslot')
            ->setParameter('timeslot', $timeslot->getId())
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Validation[] Returns an array of Validation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Validation
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
