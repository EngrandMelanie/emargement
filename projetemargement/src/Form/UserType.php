<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Promotion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormTypeInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('email')
            ->add('FirstName')
            ->add('LastName')
            ->add('roles', ChoiceType::class, [
                'required' => true,
                'multiple' => true,
                'expanded' => false,
                'choices'  => [
                    'Admin' => 'ROLE_ADMIN',
                    'Formateur' =>'ROLE_MANAGER',
                    'Etudiant' =>'ROLE_USER',
                ],
            ])
            // ->add('password')
            ->add('promotions', EntityType::class, [
                'class' => Promotion::class,
                'expanded' => true,
                'multiple' => true,
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
