<?php

namespace App\Form;

use App\Entity\TimeSlot;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Promotion;

class TimeSlotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Start', DateTimeType::class)
            ->add('End', DateTimeType::class)
            ->add('Promotion', EntityType::class, [
                'class' => Promotion::class,
                'choice_label' => 'promotion_name',
                'query_builder' => function (EntityRepository $er){
                return $er->createQueryBuilder('p')->orderBy('p.PromotionName', 'ASC');
                }
            ])
            ->add('Repeat', NumberType::class, [
                'required'   => false,
                'mapped' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
