<?php

namespace App\Form;

use App\Entity\Promotion;
use App\Entity\TimeSlot;
use App\Entity\Validation;
use App\Repository\TimeSlotRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SignatureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add('Presence', HiddenType::class)*/
            /*->add('user')*/
            /*->add('TimeSlot', EntityType::class, [
                'class' => TimeSlot::class,
                'choice_label' => function ($timeslot) {
                    return $timeslot->getId();
                }])*/
            ->add('TimeSlot', IntegerType::class,
                ['required' => true,
                ])
            ->add('Code', IntegerType::class,
                ['required' => true,
                ]);
    }

    /*public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TimeSlot::class,
        ]);
    }*/
}
